//
//  rankHeaderView.h
//  WYLiveShopping
//
//  Created by IOS1 on 2020/7/4.
//  Copyright © 2020 IOS1. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface rankHeaderView : UIView
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *oneImgV;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *oneName;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *headerimg1;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *oneNums;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *twoImgV;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *twoNumsL;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *twoNameL;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *threeImgV;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *threeNumsL;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *threeNameL;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *headerimg2;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *headerimg3;

@end

NS_ASSUME_NONNULL_END
